package com.example.dogapp.data

import com.example.dogapp.data.models.Breed
import com.example.dogapp.data.models.LikedPic
import com.example.dogapp.domain.repository.BreedRepository
import javax.inject.Inject


class BreedRepositoryImplementation @Inject constructor(
    private val breedAPIHelper: BreedAPIHelper,
    private val likedPicDao: LikedPicDao
) : BreedRepository {
    override suspend fun getBreeds(): List<Breed> {
        try {
            val resp = breedAPIHelper.getAllBreeds()
            return if (resp.isSuccessful) {
                resp.body()!!.toBreedList()
            } else return listOf()
        } catch (e: Exception) {
            return listOf()
        }

    }

    override suspend fun getRandomImageOfBreed(breed: Breed): String {
        try {
            val resp = breedAPIHelper.getRandomImageFromBreed(breed)
            return if (resp.isSuccessful) {
                resp.body()!!.toImageLink()
            } else return ""
        } catch (e: Exception) {
            return ""
        }
    }

    override suspend fun likePicture(breed: Breed, url: String): Boolean {
        val pic = LikedPic(0, breed.name, url, true)
        likedPicDao.insertAll(pic)
        return true
    }

    override suspend fun unlikePicture(url: String): Boolean {
        val pic = likedPicDao.findByUrl(url)
        likedPicDao.delete(pic)
        return true
    }

    override suspend fun checkIsLiked(breed: Breed, url: String): Boolean {
        val pic = likedPicDao.findByUrl(url)
        return !(pic == null)
    }

    override suspend fun getLikedBreeds(): List<Breed> {
        val breeds = likedPicDao.findUniqueBreeds().map { it -> Breed(it) }
        return breeds
    }

    override suspend fun getLikedPicturesByBreed(breed: Breed): List<LikedPic> {
        val pics = likedPicDao.findByBreed(breed.name)
        return pics
    }
}