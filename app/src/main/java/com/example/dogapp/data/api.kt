package com.example.dogapp.data

import com.example.dogapp.data.models.Breed
import kotlinx.coroutines.*
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.http.GET
import retrofit2.http.Path
import javax.inject.Inject


class AllBreedsResponse(val message: Map<String, List<String>>, val status: String) {
    override fun toString(): String {
        return status
    }

    fun toBreedList(): List<Breed> {
        val ans: MutableList<Breed> = mutableListOf()
        for ((k, _) in message) {
            ans.add(Breed(k))
        }
        return ans
    }
}

class BreedResponse(val message: String, val status: String) {
    override fun toString(): String {
        return "code : $status - $message"
    }

    fun toImageLink(): String {
        return message
    }
}

interface BreedApi {
    @GET("breeds/list/all")
    suspend fun getAllBreeds(): Response<AllBreedsResponse>

    @GET("/api/breed/{breed}/images/random")
    suspend fun getRandomImageFromBreed(@Path("breed") breed: String)
            : Response<BreedResponse>
}

class BreedAPIHelper @Inject constructor(retrofit: Retrofit) {
    private val breedApi: BreedApi

    init {
        breedApi = retrofit.create(BreedApi::class.java)
    }

    suspend fun getAllBreeds(): Response<AllBreedsResponse> {
        val resp = breedApi.getAllBreeds()
        return resp
    }

    suspend fun getRandomImageFromBreed(breed: Breed): Response<BreedResponse> {
        return breedApi.getRandomImageFromBreed(breed = breed.name)
    }
}