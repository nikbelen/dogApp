package com.example.dogapp.data.di

import android.content.Context
import androidx.room.Room
import com.example.dogapp.data.BreedAPIHelper
import com.example.dogapp.data.BreedRepositoryImplementation
import com.example.dogapp.data.LikedPicDao
import com.example.dogapp.data.PicDatabase
import com.example.dogapp.domain.repository.BreedRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DataModule {
    @Provides
    fun providesBaseUrl(): String = "https://dog.ceo/api/"

    @Provides
    fun provideClient(): OkHttpClient {
        val interceptor = HttpLoggingInterceptor()
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        return OkHttpClient.Builder().addInterceptor(interceptor).build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(BASE_URL: String, client: OkHttpClient): Retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .client(client)
        .addConverterFactory(MoshiConverterFactory.create())
        .build()

    @Provides
    @Singleton
    fun provideBreedAPIHelper(retrofit: Retrofit): BreedAPIHelper {
        return BreedAPIHelper(retrofit = retrofit)
    }

    @Singleton
    @Provides
    fun providePicDatabase(
        @ApplicationContext app: Context
    ) = Room.databaseBuilder(
        app,
        PicDatabase::class.java,
        "pic_database"
    ).fallbackToDestructiveMigration().build()

    @Provides
    @Singleton
    fun provideLikedPicDao(db: PicDatabase) = db.likedPicDao()

    @Provides
    @Singleton
    fun provideBreedRepository(
        breedAPIHelper: BreedAPIHelper,
        likedPicDao: LikedPicDao
    ): BreedRepository {
        return BreedRepositoryImplementation(
            breedAPIHelper = breedAPIHelper,
            likedPicDao = likedPicDao
        )
    }
}