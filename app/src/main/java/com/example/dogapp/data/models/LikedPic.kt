package com.example.dogapp.data.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class LikedPic(
    @PrimaryKey(autoGenerate = true) var intLikedPicId: Int,
    @ColumnInfo(name = "txtBreedName") var txtBreedName: String,
    @ColumnInfo(name = "txtPicUrl") var txtPicUrl: String,
    @ColumnInfo(name = "boolIsLiked") var boolIsLiked: Boolean
)
