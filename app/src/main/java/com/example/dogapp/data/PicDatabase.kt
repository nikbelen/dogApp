package com.example.dogapp.data

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.dogapp.data.models.LikedPic

@Database(entities = [LikedPic::class], version = 2, exportSchema = false)
abstract class PicDatabase : RoomDatabase() {
    abstract fun likedPicDao(): LikedPicDao
}