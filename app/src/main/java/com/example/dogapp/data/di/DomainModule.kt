package com.example.dogapp.data.di

import com.example.dogapp.domain.repository.BreedRepository
import com.example.dogapp.domain.usecases.CheckIsLikedUseCase
import com.example.dogapp.domain.usecases.DeleteLikedPicUseCase
import com.example.dogapp.domain.usecases.GetBreedsFromAPIUseCase
import com.example.dogapp.domain.usecases.GetLikedBreedsUseCase
import com.example.dogapp.domain.usecases.GetLikedPicturesByBreedUseCase
import com.example.dogapp.domain.usecases.GetRandomBreedImageUseCase
import com.example.dogapp.domain.usecases.SaveLikedPicUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module
@InstallIn(ViewModelComponent::class)
class DomainModule {
    @Provides
    fun provideGetBreedsFromAPIUseCase(breedRepository: BreedRepository): GetBreedsFromAPIUseCase {
        return GetBreedsFromAPIUseCase(breedRepository)
    }

    @Provides
    fun provideGetRandomBreedImageUseCase(breedRepository: BreedRepository): GetRandomBreedImageUseCase {
        return GetRandomBreedImageUseCase(breedRepository)
    }

    @Provides
    fun provideDeleteLikedPicUseCase(breedRepository: BreedRepository): DeleteLikedPicUseCase {
        return DeleteLikedPicUseCase(breedRepository)
    }

    @Provides
    fun provideSaveLikedPicUseCase(breedRepository: BreedRepository): SaveLikedPicUseCase {
        return SaveLikedPicUseCase(breedRepository)
    }

    @Provides
    fun provideCheckIsLikedUseCase(breedRepository: BreedRepository): CheckIsLikedUseCase {
        return CheckIsLikedUseCase(breedRepository)
    }

    @Provides
    fun provideGetLikedBreedsUseCase(breedRepository: BreedRepository): GetLikedBreedsUseCase {
        return GetLikedBreedsUseCase(breedRepository)
    }

    @Provides
    fun provideGetLikedPicturesByBreedUseCase(breedRepository: BreedRepository): GetLikedPicturesByBreedUseCase {
        return GetLikedPicturesByBreedUseCase(breedRepository)
    }
}