package com.example.dogapp.data.models

class Breed(val name: String) {
    override fun toString(): String {
        return name
    }
}