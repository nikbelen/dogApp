package com.example.dogapp.data

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.example.dogapp.data.models.LikedPic

@Dao
interface LikedPicDao {
    @Query("SELECT * FROM likedpic")
    fun getAll(): List<LikedPic>

    @Query(
        "SELECT * FROM likedpic WHERE txtPicUrl LIKE :picUrl" +
                " LIMIT 1"
    )
    fun findByUrl(picUrl: String): LikedPic

    @Query("SELECT DISTINCT txtBreedName FROM likedpic")
    fun findUniqueBreeds(): List<String>

    @Query("SELECT * FROM likedpic WHERE txtBreedName LIKE :breed")
    fun findByBreed(breed: String): List<LikedPic>

    @Insert
    fun insertAll(vararg likedPic: LikedPic)

    @Update
    fun updateOne(likedPic: LikedPic)

    @Delete
    fun delete(likedPic: LikedPic)
}