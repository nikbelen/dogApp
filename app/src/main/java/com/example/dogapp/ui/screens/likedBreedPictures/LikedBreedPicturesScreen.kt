package com.example.dogapp.ui.screens.likedBreedPictures

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.navigation.NavController
import com.example.dogapp.R
import com.example.dogapp.data.models.Breed
import com.example.dogapp.ui.elements.DialogWithImage
import com.example.dogapp.ui.elements.LoadingBox
import com.example.dogapp.ui.elements.NoPicturesDialog
import com.example.dogapp.ui.elements.SimpleCard

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun LikedBreedPicturesScreen(
    navController: NavController,
    dogBreed: String?,
) {
    val viewModel = hiltViewModel<LikedBreedPicturesViewModel>()
    val uiState by viewModel.likedBreedPicturesUIState.collectAsStateWithLifecycle()
    Surface(color = Color.Yellow) {
        Scaffold(
            topBar = {
                TopAppBar(
                    title = { Text(text = stringResource(id = R.string.screen_title_liked_pictures)) },
                    navigationIcon = {
                        IconButton(onClick = { navController.popBackStack() }) {
                            Icon(
                                imageVector = Icons.Filled.ArrowBack,
                                contentDescription = stringResource(id = R.string.back),
                            )
                        }
                    }
                )
            }

        ) {
            if (uiState.isLoading) {
                LoadingBox()
            } else {
                if (uiState.showDialog)
                    uiState.currentPic?.let { pic ->
                        DialogWithImage(
                            onDismissRequest = {
                                viewModel.send(LikedBreedPicturesHideDialogEvent())
                            },
                            onConfirmation = {
                                viewModel.send(
                                    LikedBreedPicturesDeleteEvent(
                                        Breed(pic.txtBreedName),
                                        pic.txtPicUrl
                                    )
                                )
                                viewModel.send(LikedBreedPicturesHideDialogEvent())
                            },
                            imageUrl = pic.txtPicUrl,
                            imageDescription = "Description",
                            dialogText = "Do you really want to delete this good boy from liked?"
                        )
                    }
                else if (uiState.likedPics?.isNotEmpty() == true)
                    LazyColumn(contentPadding = it) {
                        uiState.likedPics?.forEach { pic ->
                            item {
                                SimpleCard(
                                    text = pic.txtBreedName,
                                    drawable = pic.txtPicUrl,
                                    modifier = Modifier
                                        .padding(8.dp)
                                        .clickable(onClick = {
                                            viewModel.send(LikedBreedPicturesShowDialogEvent(pic))
                                        })
                                )
                            }
                        }
                    }
                else
                    NoPicturesDialog(
                        onDismissRequest = { navController.popBackStack() },
                        dialogText = "No more pictures of this breed left"
                    )
            }
        }
    }
}
