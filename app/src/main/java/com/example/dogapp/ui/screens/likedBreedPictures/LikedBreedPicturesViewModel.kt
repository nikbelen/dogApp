package com.example.dogapp.ui.screens.likedBreedPictures

import android.util.Log
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.dogapp.data.models.Breed
import com.example.dogapp.data.models.LikedPic
import com.example.dogapp.domain.usecases.DeleteLikedPicUseCase
import com.example.dogapp.domain.usecases.GetLikedPicturesByBreedUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LikedBreedPicturesViewModel @Inject constructor(
    savedStateHandle: SavedStateHandle,
    private val deleteLikedPicUseCase: DeleteLikedPicUseCase,
    private val getLikedPicturesByBreedUseCase: GetLikedPicturesByBreedUseCase
) : ViewModel() {
    private val breedArg = checkNotNull(savedStateHandle.get<String>("dogBreed"))
    private val _likedBreedPicturesUIState = MutableStateFlow(LikedBreedPicturesUIState())
    val likedBreedPicturesUIState: StateFlow<LikedBreedPicturesUIState> =
        _likedBreedPicturesUIState.asStateFlow()

    init {
        send(LikedBreedPicturesFetchEvent(Breed(breedArg)))
    }

    fun send(event: LikedBreedPicturesEvent) {
        when (event) {
            is LikedBreedPicturesFetchEvent -> {
                fetchPictures(event.breed)
            }

            is LikedBreedPicturesDeleteEvent -> {
                deleteImage(event.breed, event.url)
            }

            is LikedBreedPicturesShowDialogEvent -> {
                showDialog(event.pic)
            }

            is LikedBreedPicturesHideDialogEvent -> {
                hideDialog()
            }
        }
    }

    private fun fetchPictures(breed: Breed) {
        Log.d("AAA", breed.name)
        viewModelScope.launch(Dispatchers.IO) {
            _likedBreedPicturesUIState.update { likedBreedPicturesUIState: LikedBreedPicturesUIState ->
                likedBreedPicturesUIState.copy(isLoading = true)
            }
            val likedPics = getLikedPicturesByBreedUseCase.execute(breed)
            _likedBreedPicturesUIState.update { likedBreedPicturesUIState: LikedBreedPicturesUIState ->
                likedBreedPicturesUIState.copy(isLoading = false, likedPics = likedPics)
            }
        }
    }

    private fun showDialog(pic: LikedPic) {
        viewModelScope.launch(Dispatchers.IO) {
            _likedBreedPicturesUIState.update { likedBreedPicturesUIState: LikedBreedPicturesUIState ->
                likedBreedPicturesUIState.copy(
                    isLoading = false,
                    currentPic = pic,
                    showDialog = true
                )
            }
        }
    }

    private fun hideDialog() {
        viewModelScope.launch(Dispatchers.IO) {
            _likedBreedPicturesUIState.update { likedBreedPicturesUIState: LikedBreedPicturesUIState ->
                likedBreedPicturesUIState.copy(isLoading = false, showDialog = false)
            }
        }
    }

    private fun deleteImage(breed: Breed, url: String) {
        viewModelScope.launch(Dispatchers.IO) {
            _likedBreedPicturesUIState.update { likedBreedPicturesUIState: LikedBreedPicturesUIState ->
                likedBreedPicturesUIState.copy(isLoading = true)
            }
            deleteLikedPicUseCase.execute(url)
            _likedBreedPicturesUIState.update { likedBreedPicturesUIState: LikedBreedPicturesUIState ->
                likedBreedPicturesUIState.copy(isLoading = false)
            }
            fetchPictures(breed)
            Log.d("AAA", url)
        }
    }
}