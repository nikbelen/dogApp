package com.example.dogapp.ui.screens.likedBreeds

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.dogapp.data.models.Breed
import com.example.dogapp.domain.usecases.GetLikedBreedsUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LikedBreedsViewModel @Inject constructor(
    private val getLikedBreedsUseCase: GetLikedBreedsUseCase
) : ViewModel() {
    private val _LikedBreedsUIState = MutableStateFlow(LikedBreedsUIState())

    val likedBreedsUiState: StateFlow<LikedBreedsUIState> = _LikedBreedsUIState.asStateFlow()

    init {
        Log.d("AAA", "VM created")
        send(LikedBreedsGetEvent())
    }

    fun send(event: LikedBreedsEvent) {
        when (event) {
            is LikedBreedsGetEvent -> {
                fetchAllBreeds()
            }

            is LikedBreedsSetEvent -> {
                setCurrentBreed(event.breed)
            }
        }
    }

    private fun setCurrentBreed(breed: Breed) {
        Log.d("AAA", breed.name)
        _LikedBreedsUIState.update { likedBreedsUiState: LikedBreedsUIState ->
            likedBreedsUiState.copy(currentBreed = breed)
        }
    }

    private fun fetchAllBreeds() {
        viewModelScope.launch(Dispatchers.IO) {
            val breeds = getLikedBreedsUseCase.execute()
            _LikedBreedsUIState.update { likedBreedsUiState: LikedBreedsUIState ->
                likedBreedsUiState.copy(breeds = breeds, breedImages = null, isLoading = false)
            }
        }
    }
}