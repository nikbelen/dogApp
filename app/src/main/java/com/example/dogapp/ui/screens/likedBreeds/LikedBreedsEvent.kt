package com.example.dogapp.ui.screens.likedBreeds

import com.example.dogapp.data.models.Breed

interface LikedBreedsEvent

class LikedBreedsGetEvent : LikedBreedsEvent

class LikedBreedsSetEvent(val breed: Breed) : LikedBreedsEvent