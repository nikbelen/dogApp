package com.example.dogapp.ui.screens.allBreeds

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.dogapp.data.models.Breed
import com.example.dogapp.domain.usecases.GetBreedsFromAPIUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AllBreedsViewModel @Inject constructor(
    private val getBreedsFromAPIUseCase: GetBreedsFromAPIUseCase
) : ViewModel() {
    private val _AllBreedsUIState = MutableStateFlow(AllBreedsUIState())

    val allBreedsUiState: StateFlow<AllBreedsUIState> = _AllBreedsUIState.asStateFlow()

    init {
        send(AllBreedsGetEvent())
    }

    fun send(event: AllBreedsEvent) {
        when (event) {
            is AllBreedsGetEvent -> {
                fetchAllBreeds()
            }

            is AllBreedsSetEvent -> {
                setCurrentBreed(breed = event.breed)
            }
        }
    }

    private fun setCurrentBreed(breed: Breed) {
        Log.d("AAA", breed.name)
        _AllBreedsUIState.update { allBreedsUiState: AllBreedsUIState ->
            allBreedsUiState.copy(currentBreed = breed)
        }
    }

    private fun fetchAllBreeds() {
        viewModelScope.launch {
            val breeds = getBreedsFromAPIUseCase.execute()
            _AllBreedsUIState.update { allBreedsUiState: AllBreedsUIState ->
                allBreedsUiState.copy(breeds = breeds, breedImages = null, isLoading = false)
            }
        }
    }
}