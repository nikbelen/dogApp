package com.example.dogapp.ui.screens

sealed class Screen(val route: String) {
    object Home : Screen("home_screen")
    object LikedBreeds : Screen("liked_breeds")
    object About : Screen("about_screen")

    object LikedBreedPictures : Screen("liked_pictures")
}