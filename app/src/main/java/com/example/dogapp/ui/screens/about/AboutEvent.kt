package com.example.dogapp.ui.screens.about

import com.example.dogapp.data.models.Breed

interface AboutEvent

class AboutFetchEvent(val breed: Breed) : AboutEvent

class AboutSetLikedEvent(val isLiked: Boolean) : AboutEvent

class AboutSaveEvent(val breed: Breed, val url: String) : AboutEvent

class AboutDeleteEvent(val breed: Breed, val url: String) : AboutEvent