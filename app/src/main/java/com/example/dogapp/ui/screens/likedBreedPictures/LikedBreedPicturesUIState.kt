package com.example.dogapp.ui.screens.likedBreedPictures

import com.example.dogapp.data.models.Breed
import com.example.dogapp.data.models.LikedPic

data class LikedBreedPicturesUIState(
    var breeds: List<Breed>? = null,
    var likedPics: List<LikedPic>? = null,
    val isLoading: Boolean = true,
    val currentBreed: Breed? = null,
    val currentPic: LikedPic? = null,
    val showDialog: Boolean = false
)