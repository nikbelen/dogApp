package com.example.dogapp.ui.screens.likedBreedPictures

import com.example.dogapp.data.models.Breed
import com.example.dogapp.data.models.LikedPic

interface LikedBreedPicturesEvent
class LikedBreedPicturesShowDialogEvent(val pic: LikedPic) : LikedBreedPicturesEvent
class LikedBreedPicturesHideDialogEvent : LikedBreedPicturesEvent
class LikedBreedPicturesFetchEvent(val breed: Breed) : LikedBreedPicturesEvent
class LikedBreedPicturesDeleteEvent(val breed: Breed, val url: String) : LikedBreedPicturesEvent