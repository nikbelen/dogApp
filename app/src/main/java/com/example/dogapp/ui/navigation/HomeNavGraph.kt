package com.example.dogapp.ui.navigation

import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavType
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import androidx.navigation.navigation
import com.example.dogapp.ui.screens.Screen
import com.example.dogapp.ui.screens.about.AboutScreen
import com.example.dogapp.ui.screens.allBreeds.AllBreedsScreen

fun NavGraphBuilder.homeNavGraph(navController: NavController) {
    navigation(
        startDestination = Screen.Home.route,
        route = HOME_GRAPH_ROUTE
    ) {
        composable(Screen.Home.route) {
            AllBreedsScreen(navController)
        }
        composable(
            "${Screen.About.route}/{dogBreed}",
            arguments = listOf(navArgument("dogBreed") { type = NavType.StringType })
        ) {
            AboutScreen(navController, it.arguments?.getString("dogBreed"))
        }
    }
}