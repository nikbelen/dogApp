package com.example.dogapp.ui

import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.navigation.compose.rememberNavController
import com.example.dogapp.ui.navigation.BottomNavigationBar
import com.example.dogapp.ui.navigation.Navigation
import com.example.dogapp.ui.theme.DogAppTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    // trying to add hilt
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Log.d("AAA", "Activity created")
//        val picDatabase = Room.databaseBuilder(
//            applicationContext,
//            PicDatabase::class.java, "database-name"
//        ).fallbackToDestructiveMigration().build()

        setContent {
            DogAppTheme {
                Surface(color = Color.Yellow) {
                    val navController = rememberNavController()
                    Scaffold(
                        bottomBar = { BottomNavigationBar(navController) }
                    ) {
                        Navigation(navController = navController, Modifier.padding(it))
                    }
                }
            }
        }
    }
}