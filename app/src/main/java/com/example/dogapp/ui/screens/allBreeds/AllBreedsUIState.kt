package com.example.dogapp.ui.screens.allBreeds

import com.example.dogapp.data.models.Breed

data class AllBreedsUIState(
    var breeds: List<Breed>? = null,
    var breedImages: Map<Breed, String>? = null,
    val isLoading: Boolean = true,
    val currentBreed: Breed? = null
)