package com.example.dogapp.ui.screens.likedBreeds

import com.example.dogapp.data.models.Breed

data class LikedBreedsUIState(
    var breeds: List<Breed>? = null,
    var breedImages: Map<Breed, String>? = null,
    val isLoading: Boolean = true,
    val currentBreed: Breed? = null
)