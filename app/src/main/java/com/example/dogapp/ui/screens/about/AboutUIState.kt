package com.example.dogapp.ui.screens.about

import com.example.dogapp.data.models.Breed

data class AboutUIState(
    var breeds: List<Breed>? = null,
    val isLiked: Boolean = false,
    val isLoading: Boolean = true,
    val currentBreed: Breed? = null,
    val dogURL: String? = null
)