package com.example.dogapp.ui.screens.allBreeds

import com.example.dogapp.data.models.Breed

interface AllBreedsEvent

class AllBreedsGetEvent : AllBreedsEvent

class AllBreedsSetEvent(val breed: Breed) : AllBreedsEvent