package com.example.dogapp.ui.screens.about

import android.annotation.SuppressLint
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.ArrowForward
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.FavoriteBorder
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.navigation.NavController
import coil.compose.rememberAsyncImagePainter
import com.example.dogapp.R
import com.example.dogapp.data.models.Breed
import com.example.dogapp.ui.elements.CenteredText
import com.example.dogapp.ui.elements.DogImageBox
import com.example.dogapp.ui.elements.LoadingBox
import com.example.dogapp.ui.elements.SimpleIconButton
import com.example.dogapp.utils.capitaliseFirst

@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AboutScreen(
    navController: NavController,
    dogBreed: String?,
) {
    val viewModel = hiltViewModel<AboutViewModel>()
    val uiState by viewModel.aboutUIState.collectAsStateWithLifecycle()
    Scaffold(
        topBar = {
            TopAppBar(
                title = { Text(text = stringResource(id = R.string.screen_title_about)) },
                navigationIcon = {
                    IconButton(onClick = { navController.popBackStack() }) {
                        Icon(
                            imageVector = Icons.Filled.ArrowBack,
                            contentDescription = stringResource(id = R.string.back),
                        )
                    }
                }
            )
        }

    ) {
        if (uiState.isLoading) {
            LoadingBox()
        } else {
            LazyColumn() {
                uiState.currentBreed?.let {
                    item {
                        Text(
                            text = it.name.capitaliseFirst(),
                            style = MaterialTheme.typography.titleMedium,
                            modifier = Modifier.background(Color.Black)
                        )
                    }
                }

                item {
                    DogImageBox(
                        rememberAsyncImagePainter(
                            uiState.dogURL,
                            error = painterResource(R.drawable.gooseling)
                        )
                    )
                }

                item {
                    uiState.currentBreed?.let {
                        CenteredText(text = it.name)
                    }
                }

                item {
                    Row(
                        modifier = Modifier.fillMaxWidth(),
                        verticalAlignment = Alignment.CenterVertically,
                        horizontalArrangement = Arrangement.Absolute.SpaceEvenly
                    ) {
                        SimpleIconButton(
                            icon = when (uiState.isLiked) {
                                true -> Icons.Default.Favorite
                                false -> Icons.Default.FavoriteBorder
                            },
                            text = "Like",
                            onClick = {
                                if (uiState.isLiked)
                                    uiState.dogURL?.let { url ->
                                        dogBreed?.let { breedStr -> Breed(breedStr) }
                                            ?.let { breed ->
                                                viewModel.send(
                                                    AboutDeleteEvent(
                                                        breed,
                                                        url
                                                    )
                                                )
                                            }
                                    }
                                else uiState.dogURL?.let { url ->
                                    dogBreed?.let { breedStr -> Breed(breedStr) }?.let { breed ->
                                        viewModel.send(
                                            AboutSaveEvent(
                                                breed,
                                                url
                                            )
                                        )
                                    }
                                }
                                viewModel.setIsLiked(!uiState.isLiked)
                            }
                        )
                        SimpleIconButton(
                            icon = Icons.Default.ArrowForward,
                            text = "Next",
                            onClick = {
                                dogBreed?.let { breedStr -> Breed(breedStr) }?.let { breed ->
                                    viewModel.send(
                                        AboutFetchEvent(
                                            breed
                                        )
                                    )
                                }
                            })
                    }

                }
            }
        }
    }
}