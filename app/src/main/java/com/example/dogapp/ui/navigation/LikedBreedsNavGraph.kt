package com.example.dogapp.ui.navigation

import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import androidx.navigation.navigation
import com.example.dogapp.ui.screens.Screen
import com.example.dogapp.ui.screens.likedBreedPictures.LikedBreedPicturesScreen
import com.example.dogapp.ui.screens.likedBreeds.LikedBreedsScreen

fun NavGraphBuilder.likedBreedsNavGraph(
    navController: NavHostController
) {
    navigation(
        startDestination = Screen.LikedBreeds.route,
        route = LIKED_BREEDS_GRAPH_ROUTE
    ) {
        composable(Screen.LikedBreeds.route) {
            LikedBreedsScreen(navController)
        }
        composable(
            "${Screen.LikedBreedPictures.route}/{dogBreed}",
            arguments = listOf(navArgument("dogBreed") { type = NavType.StringType })
        ) {
            LikedBreedPicturesScreen(navController, it.arguments?.getString("dogBreed"))
        }
    }
}