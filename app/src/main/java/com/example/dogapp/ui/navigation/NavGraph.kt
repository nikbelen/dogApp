package com.example.dogapp.ui.navigation

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost

const val HOME_GRAPH_ROUTE = "home"
const val LIKED_BREEDS_GRAPH_ROUTE = "settings"

@Composable
fun Navigation(navController: NavHostController, modifier: Modifier) {
    NavHost(
        navController = navController,
        startDestination = HOME_GRAPH_ROUTE,
        modifier = modifier
    ) {
        homeNavGraph(navController = navController)
        likedBreedsNavGraph(navController = navController)
    }
}