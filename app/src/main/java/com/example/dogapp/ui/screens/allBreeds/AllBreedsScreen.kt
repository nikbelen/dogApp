package com.example.dogapp.ui.screens.allBreeds

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.navigation.NavController
import com.example.dogapp.R
import com.example.dogapp.ui.elements.LoadingBox
import com.example.dogapp.ui.elements.SearchBar
import com.example.dogapp.ui.elements.SimpleCard
import com.example.dogapp.ui.screens.Screen

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AllBreedsScreen(
    navController: NavController,
    modifier: Modifier = Modifier,
) {
    val viewModel = hiltViewModel<AllBreedsViewModel>()
    val uiState by viewModel.allBreedsUiState.collectAsStateWithLifecycle()
    Surface(color = Color.Yellow) {
        Scaffold(
            topBar = {
                TopAppBar(
                    title = { Text(text = stringResource(id = R.string.screen_title_breed_list)) }
                )
            }
        ) {
            if (uiState.isLoading) {
                LoadingBox()
            } else if (uiState.breeds?.isNotEmpty() == true) {
                LazyColumn(contentPadding = it) {
                    item {
                        SearchBar()
                    }

                    uiState.breeds?.forEach { breed ->
                        item {
                            SimpleCard(
                                text = breed.name,
                                drawable = "https://images.dog.ceo/breeds/hound-blood/n02088466_1555.jpg",
                                modifier = Modifier
                                    .padding(8.dp)
                                    .clickable(onClick = {
                                        viewModel.send(AllBreedsSetEvent(breed = breed))
                                        navController.navigate("${Screen.About.route}/${breed.name}")
                                    })
                            )
                        }
                    }
                }
            } else Box(
                contentAlignment = Alignment.Center,
                modifier = Modifier
                    .fillMaxSize()
                    .background(Color.White)
            ) {
                Text(
                    text = "Some error occurred when getting list of likeable breeds, try again later!",
                    textAlign = TextAlign.Center
                )
            }
        }
    }
}