package com.example.dogapp.ui.screens.about

import android.util.Log
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.dogapp.data.models.Breed
import com.example.dogapp.domain.usecases.CheckIsLikedUseCase
import com.example.dogapp.domain.usecases.DeleteLikedPicUseCase
import com.example.dogapp.domain.usecases.GetRandomBreedImageUseCase
import com.example.dogapp.domain.usecases.SaveLikedPicUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AboutViewModel @Inject constructor(
    savedStateHandle: SavedStateHandle,
    private val getRandomBreedImageUseCase: GetRandomBreedImageUseCase,
    private val saveLikedPicUseCase: SaveLikedPicUseCase,
    private val deleteLikedPicUseCase: DeleteLikedPicUseCase,
    private val checkIsLikedUseCase: CheckIsLikedUseCase
) : ViewModel() {
    private val breedArg = checkNotNull(savedStateHandle.get<String>("dogBreed"))
    private val _aboutUIState = MutableStateFlow(AboutUIState())
    val aboutUIState: StateFlow<AboutUIState> = _aboutUIState.asStateFlow()

    init {
        send(AboutFetchEvent(Breed(breedArg)))
    }

    fun setIsLiked(value: Boolean) {
        viewModelScope.launch {
            _aboutUIState.update { aboutUIState: AboutUIState ->
                aboutUIState.copy(isLiked = value)
            }
        }
    }

    fun send(event: AboutEvent) {
        when (event) {
            is AboutFetchEvent -> {
                fetchImage(event.breed)
            }

            is AboutSetLikedEvent -> {
                setIsLiked(event.isLiked)
            }

            is AboutSaveEvent -> {
                saveImage(event.breed, event.url)
            }

            is AboutDeleteEvent -> {
                deleteImage(event.breed, event.url)
            }
        }
    }

    private fun fetchImage(breed: Breed) {
        Log.d("AAA", breed.name)
        viewModelScope.launch(Dispatchers.IO) {
            _aboutUIState.update { aboutUIState: AboutUIState ->
                aboutUIState.copy(currentBreed = breed, dogURL = null, isLoading = true)
            }
            val url = getRandomBreedImageUseCase.execute(breed)
            val isLiked = checkIsLikedUseCase.execute(breed, url)
            _aboutUIState.update { aboutUIState: AboutUIState ->
                aboutUIState.copy(
                    currentBreed = breed,
                    dogURL = url,
                    isLoading = false,
                    isLiked = isLiked
                )
            }
            Log.d("AAA", url)
        }
    }

    private fun saveImage(breed: Breed, url: String) {
        viewModelScope.launch(Dispatchers.IO) {
            _aboutUIState.update { aboutUIState: AboutUIState ->
                aboutUIState.copy(currentBreed = breed, dogURL = url, isLoading = true)
            }
            saveLikedPicUseCase.execute(breed, url)
            _aboutUIState.update { aboutUIState: AboutUIState ->
                aboutUIState.copy(
                    currentBreed = breed,
                    dogURL = url,
                    isLoading = false,
                    isLiked = true
                )
            }
            Log.d("AAA", url)
        }
    }

    private fun deleteImage(breed: Breed, url: String) {
        viewModelScope.launch(Dispatchers.IO) {
            _aboutUIState.update { aboutUIState: AboutUIState ->
                aboutUIState.copy(currentBreed = breed, dogURL = url, isLoading = true)
            }
            deleteLikedPicUseCase.execute(url)
            _aboutUIState.update { aboutUIState: AboutUIState ->
                aboutUIState.copy(
                    currentBreed = breed,
                    dogURL = url,
                    isLoading = false,
                    isLiked = false
                )
            }
            Log.d("AAA", url)
        }
    }
}