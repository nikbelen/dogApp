package com.example.dogapp.domain.usecases

import com.example.dogapp.data.models.Breed
import com.example.dogapp.domain.repository.BreedRepository
import javax.inject.Inject

class GetBreedsFromAPIUseCase @Inject constructor(private val breedRepository: BreedRepository) {
    suspend fun execute(): List<Breed> {
        return breedRepository.getBreeds()
    }
}