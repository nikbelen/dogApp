package com.example.dogapp.domain.usecases

import com.example.dogapp.domain.repository.BreedRepository
import javax.inject.Inject

class DeleteLikedPicUseCase @Inject constructor(private val breedRepository: BreedRepository) {
    suspend fun execute(url: String): Boolean {
        return breedRepository.unlikePicture(url)
    }
}