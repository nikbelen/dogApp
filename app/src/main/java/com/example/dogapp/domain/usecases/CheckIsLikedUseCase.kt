package com.example.dogapp.domain.usecases

import com.example.dogapp.data.models.Breed
import com.example.dogapp.domain.repository.BreedRepository
import javax.inject.Inject

class CheckIsLikedUseCase @Inject constructor(private val breedRepository: BreedRepository) {
    suspend fun execute(breed: Breed, url: String): Boolean {
        return breedRepository.checkIsLiked(breed, url)
    }
}