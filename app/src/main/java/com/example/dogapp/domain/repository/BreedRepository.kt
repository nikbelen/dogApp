package com.example.dogapp.domain.repository

import com.example.dogapp.data.models.Breed
import com.example.dogapp.data.models.LikedPic

interface BreedRepository {
    suspend fun getBreeds(): List<Breed>

    suspend fun getRandomImageOfBreed(breed: Breed): String

    suspend fun likePicture(breed: Breed, url: String): Boolean

    suspend fun unlikePicture(url: String): Boolean

    suspend fun checkIsLiked(breed: Breed, url: String): Boolean

    suspend fun getLikedBreeds(): List<Breed>

    suspend fun getLikedPicturesByBreed(breed: Breed): List<LikedPic>

}