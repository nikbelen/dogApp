package com.example.dogapp.domain.usecases

import com.example.dogapp.data.models.Breed
import com.example.dogapp.data.models.LikedPic
import com.example.dogapp.domain.repository.BreedRepository
import javax.inject.Inject

class GetLikedPicturesByBreedUseCase @Inject constructor(private val breedRepository: BreedRepository) {
    suspend fun execute(breed: Breed): List<LikedPic> {
        return breedRepository.getLikedPicturesByBreed(breed)
    }
}