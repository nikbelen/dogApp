package com.example.dogapp.domain.usecases

import com.example.dogapp.data.models.Breed
import com.example.dogapp.domain.repository.BreedRepository
import javax.inject.Inject

class GetRandomBreedImageUseCase @Inject constructor(private val breedRepository: BreedRepository) {
    suspend fun execute(breed: Breed): String {
        return breedRepository.getRandomImageOfBreed(breed)
    }
}